import sys
sys.setrecursionlimit(1025)

def climb_stairs(n, memo={}):
    if n == 1:
        return 1
    if n == 2:
        return 2
    if n in memo:
        return memo[n]

    memo[n] = climb_stairs(n - 1, memo) + climb_stairs(n - 2, memo)
    return memo[n]


print(climb_stairs(1024))


#这个递归函数的思路是:

#   如果只有1个台阶,那么就只有1种方法爬上去。
#    如果有2个台阶,那么就有2种方法:爬1步或爬2步。
#   对于n个台阶,最后一步可以从n-1爬1步上来,或者从n-2爬2步上来。
#    所以f(n) = f(n-1) + f(n-2),这就是典型的斐波那契数列。

#对于1024个台阶,递归会重复计算很多次。我们可以用一个数组来存储已经计算过的结果,避免重复计算: